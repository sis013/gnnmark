2021-03-12 13:29:31,566 Namespace(add_virtual_node=False, batch_size=256, block='res+', conv='gen', conv_encode_edge=True, dataset='ogbg-molhiv', device=0, dropout=0.2, epochs=1, feature='full', gcn_aggr='softmax', gpus=1, graph_pooling='mean', hidden_channels=256, learn_msg_scale=False, learn_p=False, learn_t=True, lr=0.01, mlp_layers=1, model_load_path='ogbg_molhiv_pretrained_model.pth', model_save_path='log/EXP-B_res+-C_gen-L_28-F_256-DP_0.2-GA_softmax-T_1.0-LT_True-P_1.0-LP_False-MN_False-LS_False-20210312-132925-2921e611-8642-4bb4-bd30-03ab29e628b8/model_ckpt', msg_norm=False, nodes=1, norm='batch', nr=0, num_layers=28, num_tasks=1, num_workers=0, p=1.0, save='log/EXP-B_res+-C_gen-L_28-F_256-DP_0.2-GA_softmax-T_1.0-LT_True-P_1.0-LP_False-MN_False-LS_False-20210312-132925-2921e611-8642-4bb4-bd30-03ab29e628b8', t=1.0, use_gpu=False, world_size=1)
2021-03-12 13:29:58,708 DeeperGCN(
  (gcns): ModuleList(
    (0): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (1): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (2): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (3): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (4): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (5): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (6): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (7): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (8): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (9): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (10): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (11): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (12): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (13): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (14): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (15): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (16): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (17): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (18): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (19): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (20): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (21): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (22): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (23): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (24): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (25): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (26): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
    (27): GENConv(
      (mlp): MLP(
        (0): Linear(in_features=256, out_features=256, bias=True)
      )
      (msg_encoder): ReLU()
      (edge_encoder): BondEncoder(
        (bond_embedding_list): ModuleList(
          (0): Embedding(5, 256)
          (1): Embedding(6, 256)
          (2): Embedding(2, 256)
        )
      )
    )
  )
  (norms): ModuleList(
    (0): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (1): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (2): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (3): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (4): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (5): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (6): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (7): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (8): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (9): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (10): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (11): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (12): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (13): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (14): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (15): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (16): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (17): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (18): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (19): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (20): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (21): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (22): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (23): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (24): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (25): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (26): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (27): BatchNorm1d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  )
  (atom_encoder): AtomEncoder(
    (atom_embedding_list): ModuleList(
      (0): Embedding(119, 256)
      (1): Embedding(4, 256)
      (2): Embedding(12, 256)
      (3): Embedding(12, 256)
      (4): Embedding(10, 256)
      (5): Embedding(6, 256)
      (6): Embedding(6, 256)
      (7): Embedding(2, 256)
      (8): Embedding(2, 256)
    )
  )
  (graph_pred_linear): Linear(in_features=256, out_features=1, bias=True)
)
2021-03-12 13:29:58,712 =====Epoch 1
2021-03-12 13:29:58,712 Training...
